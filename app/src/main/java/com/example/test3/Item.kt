package com.example.test3

import android.os.Parcelable
import android.widget.EditText
import kotlinx.android.parcel.Parcelize
@Parcelize

data class Item(val name: String, val lastName: String, val email: String): Parcelable
