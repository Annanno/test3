package com.example.test3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val items = mutableListOf<Item>()
    private lateinit var adapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private fun init() {
        setData()
        val recycler = findViewById<RecyclerView>(R.id.recyclerView)
        recycler.layoutManager = LinearLayoutManager(this)
        adapter = Adapter(items)
        recycler.adapter = adapter


    }

    private fun setData() {
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
        items.add(Item("jemala", "lemali", "jemals"))
    }



//    fun start() {
//            val name = findViewById<TextView>(R.id.name).text.toString()
//            val lastname = findViewById<TextView>(R.id.lastname).text.toString()
//            val email = findViewById<TextView>(R.id.email).text.toString()
//            val itmes = Item(name, lastname, email)
//            val intent = Intent(this, UserActivity::class.java)
//            val resultLauncher =
//                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->}
//            resultLauncher.launch(intent)
//
//
//    }
}