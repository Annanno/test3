package com.example.test3

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView

class Adapter(private val items: MutableList<Item>) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private lateinit var model: Item

        fun onBind() {
            val model = items[adapterPosition]
            val name = itemView.findViewById<TextView>(R.id.name)
            val lastname = itemView.findViewById<TextView>(R.id.lastname)
            val email = itemView.findViewById<TextView>(R.id.email)
            val removebtn = itemView.findViewById<ImageButton>(R.id.remove)
            val updatebtn = itemView.findViewById<ImageButton>(R.id.update)

            name.text = model.name
            lastname.text = model.lastName
            email.text = model.email

            removebtn.setOnClickListener {
                items.removeAt(adapterPosition)
                notifyItemRemoved(adapterPosition)
            }
            updatebtn.setOnClickListener {

            }




        }
    }

}